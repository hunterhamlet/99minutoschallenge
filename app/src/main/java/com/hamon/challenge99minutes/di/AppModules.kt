package com.hamon.challenge99minutes.di


import com.hamon.challenge99minutes.viewmodel.DetailPlaceViewModel
import com.hamon.challenge99minutes.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModules = module {
    viewModel { MainViewModel(get(), get(), get(), get(), get(), get(), get()) }
    viewModel { DetailPlaceViewModel(get(), get(), get()) }
}