package com.hamon.challenge99minutes

import androidx.multidex.MultiDexApplication
import com.hamon.challenge99minutes.di.appModules
import com.hamon.challenge99minutes.utils.ConnectivityUtil
import com.hamon.challenge99minutes.utils.LocationUtil
import com.hamon.provider.database.ObjectBox
import com.hamon.provider.di.databaseModule
import com.hamon.provider.di.databaseUseCasesModules
import com.hamon.provider.di.networkingUsesCases
import com.hamon.provider.di.repositoryModules
import com.hamon.provider.di.servicesModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            androidFileProperties()
            koin.loadModules(
                arrayListOf(
                    appModules,
                    servicesModule,
                    repositoryModules,
                    networkingUsesCases,
                    databaseUseCasesModules,
                    databaseModule
                )
            )
            koin.createRootScope()
        }
        ObjectBox.init(this)
        ConnectivityUtil.init(this)
        LocationUtil.init(this)
    }
}