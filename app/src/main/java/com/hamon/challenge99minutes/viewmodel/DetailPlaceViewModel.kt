package com.hamon.challenge99minutes.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hamon.challenge99minutes.actions.UIState
import com.hamon.challenge99minutes.base.BaseViewModel
import com.hamon.challenge99minutes.utils.ConnectivityUtil
import com.hamon.challenge99minutes.utils.constants.BUNDLE_PLACE_ID
import com.hamon.challenge99minutes.utils.extensions.toPlaceDetailUI
import com.hamon.provider.models.PhotoPlaceResponse
import com.hamon.provider.models.PlaceDetailResponse
import com.hamon.provider.networking.actions.APIActions
import com.hamon.provider.usecases.database.GetDetailsFavoritePlaceUseCase
import com.hamon.provider.usecases.networking.GetPlaceDetailsUseCase
import com.hamon.provider.usecases.networking.GetPlacePhotoUseCase
import kotlinx.coroutines.launch

class DetailPlaceViewModel(
    private val getPlaceDetailsUseCase: GetPlaceDetailsUseCase,
    private val getPlacePhotoUseCase: GetPlacePhotoUseCase,
    private val getDetailsFavoritePlaceUseCase: GetDetailsFavoritePlaceUseCase
) : BaseViewModel() {

    private val _imagePlaceObservable: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val imagePlaceObservable: LiveData<String> get() = _imagePlaceObservable

    fun bundleSetup(bundle: Bundle?) {
        bundle?.let {
            getPlaceDetails(it.getString(BUNDLE_PLACE_ID))
        }
    }

    private fun getPlaceDetails(frsqId: String?) {
        viewModelScope.launch {
            frsqId?.let {
                checkConnectivityNetwork(it)
            }
        }
    }

    private suspend fun checkConnectivityNetwork(frsqId: String) {
        if (ConnectivityUtil.isConnected()) {
            handleUIState(getPlaceDetailsUseCase.invoke(frsqId = frsqId))
            handleImage(getPlacePhotoUseCase.invoke(frsqId = frsqId))
        } else {
            handleNotConnectionNetwork(frsqId)
        }
    }

    private fun handleNotConnectionNetwork(frsqId: String) {
        viewModelScope.launch {
            getDetailsFavoritePlaceUseCase(frsqId)?.let {
                _stateObservable.postValue(UIState.OnSuccess(data = it))
            }
        }
    }

    private fun handleImage(apiResult: APIActions) {
        when (apiResult) {
            is APIActions.OnSuccess<*> -> handleImageSuccess(apiResult.response as List<PhotoPlaceResponse>)
            is APIActions.OnError -> handleErrorResponse(apiResult.message)
        }
    }

    private fun handleImageSuccess(listImageResponse: List<PhotoPlaceResponse>) {
        if (listImageResponse.isNotEmpty()) {
            _imagePlaceObservable.postValue(listImageResponse.first().getURLPhoto())
        } else {
            _imagePlaceObservable.postValue("")
        }
    }

    private fun handleUIState(apiResult: APIActions) {
        when (apiResult) {
            is APIActions.OnSuccess<*> -> handleSuccessResponse((apiResult.response as PlaceDetailResponse))
            is APIActions.OnError -> handleErrorResponse(apiResult.message)
        }
    }

    private fun handleSuccessResponse(placeDetailResponse: PlaceDetailResponse) {
        placeDetailResponse.let {
            _stateObservable.postValue(UIState.OnSuccess(data = placeDetailResponse.toPlaceDetailUI()))
        }
    }

    private fun handleErrorResponse(errorMessage: String) {
        _stateObservable.postValue(UIState.OnError(message = errorMessage))
    }

}