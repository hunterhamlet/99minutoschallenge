package com.hamon.challenge99minutes.viewmodel

import androidx.lifecycle.viewModelScope
import com.hamon.challenge99minutes.actions.UIState
import com.hamon.challenge99minutes.base.BaseViewModel
import com.hamon.challenge99minutes.utils.ConnectivityUtil
import com.hamon.challenge99minutes.utils.extensions.mapToPlaceUI
import com.hamon.challenge99minutes.utils.extensions.toPlaceDetailUI
import com.hamon.provider.models.FoursquareResponse
import com.hamon.provider.models.PlaceDetailResponse
import com.hamon.provider.models.PlaceItem
import com.hamon.provider.networking.actions.APIActions
import com.hamon.provider.uiModels.PlaceUI
import com.hamon.provider.usecases.database.GetAllFavoritePlaceUseCase
import com.hamon.provider.usecases.database.RemoveFavoriteDetailPlaceUseCase
import com.hamon.provider.usecases.database.RemoveFavoritePlaceUseCase
import com.hamon.provider.usecases.database.SaveFavoriteDetailPlaceUseCase
import com.hamon.provider.usecases.database.SaveFavoritePlaceUseCase
import com.hamon.provider.usecases.networking.GetPlaceDetailsUseCase
import com.hamon.provider.usecases.networking.GetPlacesUseCase
import kotlinx.coroutines.launch

class MainViewModel(
    private val getPlacesUseCase: GetPlacesUseCase,
    private val saveFavoritePlaceUseCase: SaveFavoritePlaceUseCase,
    private val removeFavoritePlaceUseCase: RemoveFavoritePlaceUseCase,
    private val getAllFavoritePlaceUseCase: GetAllFavoritePlaceUseCase,
    private val getPlaceDetailsUseCase: GetPlaceDetailsUseCase,
    private val saveFavoriteDetailPlaceUseCase: SaveFavoriteDetailPlaceUseCase,
    private val removeFavoriteDetailPlaceUseCase: RemoveFavoriteDetailPlaceUseCase
) : BaseViewModel() {

    fun getPlaces(lat: Double = 19.3856886, lon: Double = -99.0541368) {
        viewModelScope.launch {
            checkConnectivityNetwork(lat, lon)
        }
    }

    private suspend fun checkConnectivityNetwork(lat: Double = 19.3856886, lon: Double = -99.0541368) {
        if (ConnectivityUtil.isConnected()) {
            handleUIState(getPlacesUseCase(lat, lon))
        } else {
            handleNotNetworkConnection()
        }
    }

    private fun handleNotNetworkConnection() {
        _stateObservable.postValue(UIState.OnSuccess(data = getAllFavoritePlaceUseCase.invoke()))
    }

    private fun handleUIState(apiResult: APIActions) {
        when (apiResult) {
            is APIActions.OnSuccess<*> -> handleSuccessResponse((apiResult.response as FoursquareResponse).results)
            is APIActions.OnError -> handleErrorResponse(apiResult.message)
        }
    }

    private fun handleSuccessResponse(successResponse: List<PlaceItem?>?) {
        successResponse?.let {
            val favoriteList = getAllFavoritePlaceUseCase.invoke()
            val filterItemNotNull =
                it.filterNotNull().sortedBy { placeItem -> placeItem.distance }.mapToPlaceUI()
            filterItemNotNull.forEach {
                val resultFind = favoriteList.find { itemFind -> itemFind.frsqId == it.frsqId }
                if (resultFind != null) {
                    it.isFavorite = true
                }
            }
            _stateObservable.postValue(UIState.OnSuccess(data = filterItemNotNull))
        }
    }

    private fun handleErrorResponse(errorMessage: String) {
        _stateObservable.postValue(UIState.OnError(message = errorMessage))
    }

    fun saveFavoritePlace(place: PlaceUI) {
        viewModelScope.launch {
            if (place.isFavorite) {
                saveFavoritePlaceUseCase(place)
                place.frsqId?.let {
                    getFavoriteDetails(it)
                }
            } else {
                removeFavoritePlaceUseCase(place)
                place.frsqId?.let {
                    removeFavoriteDetailPlaceUseCase(it)
                }
            }
        }
    }

    private fun getFavoriteDetails(frsqId: String) {
        viewModelScope.launch {
            handleFavoriteDetails(getPlaceDetailsUseCase(frsqId))
        }
    }

    private fun handleFavoriteDetails(apiResult: APIActions) {
        when (apiResult) {
            is APIActions.OnSuccess<*> -> handleFavoriteDetailsSuccessResponse(apiResult.response as PlaceDetailResponse)
            is APIActions.OnError -> handleErrorResponse(apiResult.message)
        }
    }

    private fun handleFavoriteDetailsSuccessResponse(placeDetailResponse: PlaceDetailResponse) {
        viewModelScope.launch {
            saveFavoriteDetailPlaceUseCase(placeDetailResponse.toPlaceDetailUI())
        }
    }

}