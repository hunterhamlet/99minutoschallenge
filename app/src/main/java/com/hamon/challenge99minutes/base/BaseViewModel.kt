package com.hamon.challenge99minutes.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hamon.challenge99minutes.actions.UIState

open class BaseViewModel: ViewModel() {

    protected val _stateObservable: MutableLiveData<UIState> by lazy {
        MutableLiveData()
    }
    val stateObservable: LiveData<UIState> get() = _stateObservable
}