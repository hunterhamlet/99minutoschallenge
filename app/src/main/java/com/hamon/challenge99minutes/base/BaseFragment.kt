package com.hamon.challenge99minutes.base

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.hamon.challenge99minutes.utils.extensions.getColor

open class BaseFragment : Fragment() {


    fun handleError(msg: String) {
        this.view?.let {
            Snackbar.make(requireContext(), it, msg, Snackbar.LENGTH_SHORT)
                .setBackgroundTint(getColor(android.R.color.holo_red_dark))
                .setTextColor(getColor(android.R.color.white))
                .show()
        }
    }

}