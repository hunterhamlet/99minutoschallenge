package com.hamon.challenge99minutes.utils.extensions

import android.view.View
import androidx.core.view.isVisible

fun View.show() {
    this.isVisible = true
}

fun View.hide() {
    this.isVisible = false
}