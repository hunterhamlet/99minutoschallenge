package com.hamon.challenge99minutes.utils.extensions

import com.hamon.provider.models.CategoriesItem
import com.hamon.provider.models.PlaceDetailResponse
import com.hamon.provider.models.PlaceItem
import com.hamon.provider.uiModels.CategoryUI
import com.hamon.provider.uiModels.PlaceDetailUI
import com.hamon.provider.uiModels.PlaceUI

fun PlaceItem.toPlaceUI(): PlaceUI {
    return PlaceUI(
        frsqId = this.fsqId,
        distance = this.distance,
        timezone = this.timezone,
        name = this.name,
        lat = this.getLat(),
        lng = this.getLng(),
        URLIcon = this.getURLIcon()
    )
}

fun List<PlaceItem>.mapToPlaceUI(): MutableList<PlaceUI> {
    return this.map { it.toPlaceUI() }.toMutableList()
}

fun PlaceDetailResponse.toPlaceDetailUI(): PlaceDetailUI {
    return PlaceDetailUI(
        frsqId = this.fsqId,
        name = this.name,
        country = this.location?.country,
        crossStreet = this.location?.crossStreet,
        formattedAddress = this.location?.formattedAddress,
        locality = this.location?.locality,
        postcode = this.location?.postcode,
        region = this.location?.region,
        categories = this.categories?.mapNotNull { it?.toCategoryUI() },
        lat = this.geocodes?.main?.latitude,
        lng = this.geocodes?.main?.longitude
    )
}

fun CategoriesItem.toCategoryUI(): CategoryUI {
    return CategoryUI(
        name = this.name,
        iconUrl = this.icon?.getURLImage()
    )
}