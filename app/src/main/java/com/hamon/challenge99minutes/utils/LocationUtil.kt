package com.hamon.challenge99minutes.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Looper
import android.util.Log
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.maps.model.LatLng

@SuppressLint("StaticFieldLeak")
object LocationUtil {

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private val locationRequest by lazy {
        LocationRequest.create().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }
    private val locationBuilder = LocationSettingsRequest.Builder().addLocationRequest(
        locationRequest
    )

    private var requestLocationUpdate: Boolean = false

    fun init(context: Context) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    }

    fun availableLocationUpdates() {
        requestLocationUpdate = true
    }

    fun getRequestLocationUpdates() = requestLocationUpdate

    @SuppressLint("MissingPermission")
    fun getLastLocation(lastLocation: (LatLng) -> Unit) {
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            Log.d("JHMM", "location: ${location}")
            //lastLocation.invoke(LatLng(location.latitude, location.longitude))
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates(locationCallback: LocationCallback) {
        fusedLocationClient?.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    fun stopLocationUpdates(locationCallback: LocationCallback) {
        fusedLocationClient?.removeLocationUpdates(locationCallback)
    }


}