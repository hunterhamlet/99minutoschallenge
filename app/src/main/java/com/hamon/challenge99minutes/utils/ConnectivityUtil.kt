package com.hamon.challenge99minutes.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo

object ConnectivityUtil {

    private var cm: ConnectivityManager? = null
    private var activeNetwork: NetworkInfo? = null
    private var currentNetwork: Network? = null

    fun init(context: Context) {
        cm = context.getSystemService(ConnectivityManager::class.java)
        cm?.let {
            activeNetwork = it.activeNetworkInfo
            currentNetwork = it.activeNetwork
        }
    }

    fun isConnected(): Boolean = activeNetwork?.isConnectedOrConnecting == true

}