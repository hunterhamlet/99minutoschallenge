package com.hamon.challenge99minutes.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.load
import com.hamon.challenge99minutes.R
import com.hamon.challenge99minutes.actions.UIState
import com.hamon.challenge99minutes.base.BaseFragment
import com.hamon.challenge99minutes.databinding.FragmentDetailPlaceBinding
import com.hamon.challenge99minutes.ui.binding.CategoryBinding
import com.hamon.challenge99minutes.utils.extensions.hide
import com.hamon.challenge99minutes.utils.extensions.show
import com.hamon.challenge99minutes.viewmodel.DetailPlaceViewModel
import com.hamon.provider.uiModels.PlaceDetailUI
import com.xwray.groupie.GroupieAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailPlaceFragment : BaseFragment() {

    private val binding: FragmentDetailPlaceBinding by lazy {
        FragmentDetailPlaceBinding.inflate(layoutInflater, null, false)
    }
    private val viewModel: DetailPlaceViewModel by viewModel()
    private val categoriesAdapter: GroupieAdapter by lazy {
        GroupieAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel.bundleSetup(arguments)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        setupObservable()
    }

    private fun setupObservable() {
        viewModel.apply {
            imagePlaceObservable.observe(viewLifecycleOwner) {
                handleImage(it)
            }
            stateObservable.observe(viewLifecycleOwner) {
                handleUIState(it)
            }
        }
    }

    private fun handleImage(urlImage: String) {
        binding.apply {
            ivPlace.load(urlImage) {
                error(R.drawable.ic_cloud_off_outline)
            }
        }
    }

    private fun handleUIState(state: UIState) {
        when (state) {
            is UIState.OnSuccess<*> -> handleDetails(state.data as PlaceDetailUI)
            is UIState.OnError -> handleError(state.message)
        }
    }

    private fun handleDetails(placeUI: PlaceDetailUI) {
        binding.apply {
            tvLocality.text = getString(R.string.locality, placeUI.locality)
            tvRegion.text = getString(R.string.region, placeUI.region)
            tvCountry.text = getString(R.string.country, placeUI.country)
            tvFormattedAddress.text = getString(R.string.direction, placeUI.formattedAddress)
            tvPlaceName.text = placeUI.name
            placeUI.categories?.map { CategoryBinding(it.iconUrl) }
                ?.let { categoriesAdapter.addAll(it.toMutableList()) }
            detailsContainer.show()
            detailLoading.root.hide()
        }
    }

    private fun setupRecycler() {
        binding.rvCategories.adapter = categoriesAdapter
    }

}