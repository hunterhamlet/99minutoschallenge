package com.hamon.challenge99minutes.ui.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.hamon.challenge99minutes.R
import com.hamon.challenge99minutes.actions.UIState
import com.hamon.challenge99minutes.base.BaseFragment
import com.hamon.challenge99minutes.databinding.FragmentMainBinding
import com.hamon.challenge99minutes.ui.adapters.PlaceAdapter
import com.hamon.challenge99minutes.utils.LocationUtil
import com.hamon.challenge99minutes.utils.constants.BUNDLE_PLACE_ID
import com.hamon.challenge99minutes.utils.extensions.hide
import com.hamon.challenge99minutes.utils.extensions.show
import com.hamon.challenge99minutes.viewmodel.MainViewModel
import com.hamon.provider.uiModels.PlaceUI
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : BaseFragment() {

    private val adapter: PlaceAdapter by lazy {
        PlaceAdapter(::handleTapPlace, ::handleFavoritePlace)
    }
    private val viewModel: MainViewModel by viewModel()

    private val binding: FragmentMainBinding by lazy {
        FragmentMainBinding.inflate(layoutInflater)
    }
    private lateinit var globalMap: GoogleMap

    private val callback = OnMapReadyCallback { googleMap ->
        googleMap.apply {
            globalMap = this
            setupViewMap()
        }
    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            locationResult ?: return
            viewModel.getPlaces(
                locationResult.lastLocation.latitude,
                locationResult.lastLocation.longitude
            )
        }
    }

    override fun onResume() {
        super.onResume()
        if (LocationUtil.getRequestLocationUpdates()) {
            LocationUtil.startLocationUpdates(locationCallback)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMap()
        setupRecyclerView()
        setupObservable()
        requestPermissions()
    }

    override fun onPause() {
        super.onPause()
        LocationUtil.stopLocationUpdates(locationCallback)
    }

    private fun setupRecyclerView() {
        binding.apply {
            rPlaces.adapter = adapter
        }
    }

    private fun handleTapPlace(place: PlaceUI) {
        findNavController().navigate(
            R.id.fragment_detail_place,
            bundleOf(Pair(BUNDLE_PLACE_ID, place.frsqId))
        )
    }

    private fun setupObservable() {
        viewModel.stateObservable.observe(viewLifecycleOwner) {
            handleStateUI(it)
        }
    }

    private fun handleStateUI(state: UIState) {
        when (state) {
            is UIState.OnSuccess<*> -> handleSuccess(state.data as MutableList<PlaceUI>)
            is UIState.OnError -> handleError(state.message)
        }
    }

    private fun handleSuccess(placeList: MutableList<PlaceUI>) {
        adapter.submitList(placeList)
        showPlaceList()
        setupMarkers(placeList)
    }

    private fun showPlaceList() {
        binding.apply {
            rPlaces.show()
            loaderList.root.hide()
        }
    }

    private fun setupMarkers(placeList: MutableList<PlaceUI>) {
        val locationPlaceList = placeList.map { LatLng(it.lat ?: 0.0, it.lng ?: 0.0) }
        placeList.forEach {
            globalMap.addMarker(
                MarkerOptions().position(LatLng(it.lat ?: 0.0, it.lng ?: 0.0)).title(it.name)
            )
        }
        val builderBounds = LatLngBounds.builder()
        locationPlaceList.forEach {
            builderBounds.include(it)
        }
        globalMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builderBounds.build(), 10))
    }

    private fun handleFavoritePlace(place: PlaceUI) {
        adapter.tapFavorite(place)
        viewModel.saveFavoritePlace(place)
    }

    private fun requestPermissions() {
        Dexter
            .withContext(requireContext())
            .withPermissions(
                mutableListOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
            .withListener(
                object : MultiplePermissionsListener {

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        report?.let {
                            if (it.areAllPermissionsGranted()) {
                                LocationUtil.availableLocationUpdates()
                            } else {
                                Toast.makeText(
                                    requireContext(),
                                    "Para ocupar esta aplicación es necesario otorgar permisos de ubicación, al menos la primera vez.",
                                    Toast.LENGTH_LONG
                                ).show()
                                requireActivity().finish()
                            }
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>?,
                        token: PermissionToken?
                    ) {
                        token?.continuePermissionRequest()
                    }
                }
            ).check()
    }

    private fun setupMap() {
        childFragmentManager.run {
            findFragmentById(R.id.map)?.let {
                val mapFragment = childFragmentManager.findFragmentById(R.id.map)
                if (mapFragment is SupportMapFragment) {
                    mapFragment.getMapAsync(callback)
                }
            }
        }
    }

    private fun setupViewMap() {
        globalMap.apply {
            uiSettings.isZoomGesturesEnabled = true
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                isMyLocationEnabled = true
                uiSettings.isMyLocationButtonEnabled = true
            }
        }
    }

}