package com.hamon.challenge99minutes.ui.binding

import android.view.View
import coil.load
import com.hamon.challenge99minutes.R
import com.hamon.challenge99minutes.databinding.ItemCategoriesBinding
import com.xwray.groupie.viewbinding.BindableItem

class CategoryBinding(private val urlIcon: String?) : BindableItem<ItemCategoriesBinding>() {

    override fun bind(viewBinding: ItemCategoriesBinding, position: Int) {
        viewBinding.apply {
            urlIcon?.let {
                ivCategory.load(it) {
                    error(R.drawable.ic_cloud_off_outline)
                }
            }
        }
    }

    override fun getLayout(): Int = R.layout.item_categories

    override fun initializeViewBinding(view: View): ItemCategoriesBinding =
        ItemCategoriesBinding.bind(view)
}