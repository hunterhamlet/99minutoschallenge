package com.hamon.challenge99minutes.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.hamon.challenge99minutes.R
import com.hamon.challenge99minutes.databinding.ItemPlaceBinding
import com.hamon.provider.uiModels.PlaceUI

class PlaceAdapter(
    private val tapPlace: (PlaceUI) -> Unit,
    private val tapFavorite: (PlaceUI) -> Unit,
    private val placeList: MutableList<PlaceUI> = mutableListOf(),
) : RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder>() {

    fun submitList(placeList: MutableList<PlaceUI>) {
        this.placeList.clear()
        this.placeList.addAll(placeList)
        notifyItemRangeChanged(0, placeList.size)
    }

    fun tapFavorite(placeFavorite: PlaceUI) {
        val favoritePlace = placeList.find { it.frsqId == placeFavorite.frsqId }
        favoritePlace?.isFavorite = favoritePlace?.isFavorite?.not() ?: false
        notifyItemRangeChanged(0, placeList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceViewHolder =
        PlaceViewHolder(
            ItemPlaceBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int) {
        holder.bind(placeList[position], tapPlace, tapFavorite)
    }

    override fun getItemCount() = placeList.size

    inner class PlaceViewHolder(private val binding: ItemPlaceBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(place: PlaceUI, tapPlace: (PlaceUI) -> Unit, tapFavorite: (PlaceUI) -> Unit) {
            binding.apply {
                tvCityName.text = place.name
                tvDistance.text =
                    binding.root.context.getString(R.string.distance, place.distance.toString())
                ivPlace.load(place.URLIcon) {
                    error(R.drawable.ic_cloud_off_outline)
                }
                cPlace.setOnClickListener { tapPlace.invoke(place) }
                handleFavorite(place.isFavorite)
                ivFavorite.setOnClickListener {
                    tapFavorite.invoke(place)
                }
            }
        }

        private fun handleFavorite(isFavorite: Boolean) {
            binding.ivFavorite.setImageDrawable(
                if (isFavorite) ContextCompat.getDrawable(
                    binding.root.context,
                    R.drawable.ic_heart
                ) else ContextCompat.getDrawable(binding.root.context, R.drawable.ic_heart_outline)
            )
        }

    }

}