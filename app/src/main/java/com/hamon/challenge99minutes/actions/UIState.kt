package com.hamon.challenge99minutes.actions

sealed class UIState {
    data class OnSuccess<T>(val data: T) : UIState()
    object Loading : UIState()
    data class OnError(val message: String) : UIState()
}