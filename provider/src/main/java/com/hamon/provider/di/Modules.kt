package com.hamon.provider.di

import com.hamon.provider.database.PlaceDetailUIObjectBox
import com.hamon.provider.database.PlaceUIObjectBox
import com.hamon.provider.networking.API
import com.hamon.provider.networking.service.IPlaceServices
import com.hamon.provider.repository.PlaceRepository
import com.hamon.provider.usecases.database.GetAllFavoritePlaceUseCase
import com.hamon.provider.usecases.database.GetAllFavoritePlaceUseCaseImpl
import com.hamon.provider.usecases.database.GetDetailsFavoritePlaceUseCase
import com.hamon.provider.usecases.database.GetDetailsFavoritePlaceUseCaseImpl
import com.hamon.provider.usecases.database.GetFavoritePlaceCountUseCase
import com.hamon.provider.usecases.database.GetFavoritePlaceCountUseCaseImpl
import com.hamon.provider.usecases.database.GetFavoritePlaceUseCase
import com.hamon.provider.usecases.database.GetFavoritePlaceUseCaseImpl
import com.hamon.provider.usecases.database.GetPlaceDetailsCountUseCase
import com.hamon.provider.usecases.database.GetPlaceDetailsCountUseCaseImpl
import com.hamon.provider.usecases.database.RemoveFavoriteDetailPlaceUseCase
import com.hamon.provider.usecases.database.RemoveFavoriteDetailPlaceUseCaseImpl
import com.hamon.provider.usecases.database.RemoveFavoritePlaceUseCase
import com.hamon.provider.usecases.database.RemoveFavoritePlaceUseCaseImpl
import com.hamon.provider.usecases.database.SaveFavoriteDetailPlaceUseCase
import com.hamon.provider.usecases.database.SaveFavoriteDetailPlaceUseCaseImpl
import com.hamon.provider.usecases.database.SaveFavoritePlaceUseCase
import com.hamon.provider.usecases.database.SaveFavoritePlaceUseCaseImpl
import com.hamon.provider.usecases.networking.GetPlaceDetailsUseCase
import com.hamon.provider.usecases.networking.GetPlaceDetailsUseCaseImpl
import com.hamon.provider.usecases.networking.GetPlacePhotoUseCase
import com.hamon.provider.usecases.networking.GetPlacePhotoUseCaseImpl
import com.hamon.provider.usecases.networking.GetPlacesUseCase
import com.hamon.provider.usecases.networking.GetPlacesUseCasesImpl
import org.koin.dsl.module

val networkingUsesCases = module {
    factory<GetPlacesUseCase> { GetPlacesUseCasesImpl(get()) }
    factory<GetPlaceDetailsUseCase> { GetPlaceDetailsUseCaseImpl(get()) }
    factory<GetPlacePhotoUseCase> { GetPlacePhotoUseCaseImpl(get()) }
}

val databaseUseCasesModules = module {
    //places
    factory<SaveFavoritePlaceUseCase> { SaveFavoritePlaceUseCaseImpl(get()) }
    factory<GetFavoritePlaceUseCase> { GetFavoritePlaceUseCaseImpl(get()) }
    factory<GetAllFavoritePlaceUseCase> { GetAllFavoritePlaceUseCaseImpl(get()) }
    factory<RemoveFavoritePlaceUseCase> { RemoveFavoritePlaceUseCaseImpl(get()) }
    factory<GetFavoritePlaceCountUseCase> { GetFavoritePlaceCountUseCaseImpl(get()) }
    //place details
    factory<GetDetailsFavoritePlaceUseCase> { GetDetailsFavoritePlaceUseCaseImpl(get()) }
    factory<SaveFavoriteDetailPlaceUseCase> { SaveFavoriteDetailPlaceUseCaseImpl(get()) }
    factory<RemoveFavoriteDetailPlaceUseCase> { RemoveFavoriteDetailPlaceUseCaseImpl(get()) }
    factory<GetPlaceDetailsCountUseCase> { GetPlaceDetailsCountUseCaseImpl(get()) }
}

val repositoryModules = module {
    single { PlaceRepository(get()) }
}

val servicesModule = module {
    single { API.getServices<IPlaceServices>() }
}

val databaseModule = module {
    single { PlaceDetailUIObjectBox }
    single { PlaceUIObjectBox }
}