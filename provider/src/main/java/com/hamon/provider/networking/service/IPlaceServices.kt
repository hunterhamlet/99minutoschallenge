package com.hamon.provider.networking.service

import com.hamon.provider.models.FoursquareResponse
import com.hamon.provider.models.PhotoPlaceResponse
import com.hamon.provider.models.PlaceDetailResponse
import com.hamon.provider.networking.URLServices
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface IPlaceServices {

    @GET(URLServices.SEARCH_PLACES)
    suspend fun searchPlaces(
        @Header("Authorization") token: String,
        @QueryMap queryMap: Map<String, String>
    ): Response<FoursquareResponse>

    @GET(URLServices.PHOTOS_PLACE)
    suspend fun getPhotosPlace(
        @Header("Authorization") token: String,
        @Path("foursquareId") foursquareId: String
    ): Response<List<PhotoPlaceResponse>>

    @GET(URLServices.DETAILS_PLACE)
    suspend fun getDetailsPlaces(
        @Header("Authorization") token: String,
        @Path("foursquareId") foursquareId: String
    ): Response<PlaceDetailResponse>

}