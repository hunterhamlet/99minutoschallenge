package com.hamon.provider.networking

object URLServices {

    //search places
    const val SEARCH_PLACES = "places/search"
    const val LAT_LNG_KEY = "ll"

    //photos
    const val PHOTOS_PLACE = "places/{foursquareId}/photos"

    //details
    const val DETAILS_PLACE = "places/{foursquareId}"
}