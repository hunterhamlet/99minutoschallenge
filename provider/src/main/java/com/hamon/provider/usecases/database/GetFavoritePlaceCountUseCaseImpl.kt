package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceUIObjectBox

internal class GetFavoritePlaceCountUseCaseImpl(private val placeUIProvider: PlaceUIObjectBox) :
    GetFavoritePlaceCountUseCase {

    override suspend operator fun invoke(): Long = placeUIProvider.countPlacesSaved()
}

interface GetFavoritePlaceCountUseCase {
    suspend operator fun invoke(): Long
}