package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceDetailUIObjectBox

internal class GetPlaceDetailsCountUseCaseImpl(private val placeUIProvider: PlaceDetailUIObjectBox) :
    GetPlaceDetailsCountUseCase {
    override fun invoke(): Long = placeUIProvider.countPlacesSaved()
}

interface GetPlaceDetailsCountUseCase {
    operator fun invoke(): Long
}