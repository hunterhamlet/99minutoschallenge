package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceUIObjectBox
import com.hamon.provider.uiModels.PlaceUI

internal class GetAllFavoritePlaceUseCaseImpl(private val placeUIProvider: PlaceUIObjectBox) :
    GetAllFavoritePlaceUseCase {
    override fun invoke(): List<PlaceUI> = placeUIProvider.getAllPlaces()
}

interface GetAllFavoritePlaceUseCase {
    operator fun invoke(): List<PlaceUI>
}