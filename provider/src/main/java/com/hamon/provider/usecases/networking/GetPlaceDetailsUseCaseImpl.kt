package com.hamon.provider.usecases.networking

import com.hamon.provider.networking.actions.APIActions
import com.hamon.provider.repository.PlaceRepository

internal class GetPlaceDetailsUseCaseImpl(private val repository: PlaceRepository) :
    GetPlaceDetailsUseCase {
    override suspend operator fun invoke(frsqId: String): APIActions =
        repository.getPlaceDetails(frsqId)
}

interface GetPlaceDetailsUseCase {
    suspend operator fun invoke(frsqId: String): APIActions
}