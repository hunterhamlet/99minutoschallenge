package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceUIObjectBox
import com.hamon.provider.uiModels.PlaceUI

internal class SaveFavoritePlaceUseCaseImpl(private val placeUIProvider: PlaceUIObjectBox) :
    SaveFavoritePlaceUseCase {
    override suspend operator fun invoke(placeUI: PlaceUI) {
        if (placeUIProvider.getPlace(placeUI.frsqId ?: "") == null) {
            placeUIProvider.savePlace(placeUI)
        }
    }
}

interface SaveFavoritePlaceUseCase {
    suspend operator fun invoke(placeUI: PlaceUI)
}