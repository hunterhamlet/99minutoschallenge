package com.hamon.provider.usecases.networking

import com.hamon.provider.networking.actions.APIActions
import com.hamon.provider.repository.PlaceRepository

internal class GetPlacePhotoUseCaseImpl(private val repository: PlaceRepository) :
    GetPlacePhotoUseCase {
    override suspend operator fun invoke(frsqId: String): APIActions = repository.getPlacePhoto(frsqId)
}

interface GetPlacePhotoUseCase {
    suspend operator fun invoke(frsqId: String): APIActions
}

