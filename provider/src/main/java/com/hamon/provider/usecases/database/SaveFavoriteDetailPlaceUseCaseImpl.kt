package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceDetailUIObjectBox
import com.hamon.provider.uiModels.PlaceDetailUI

internal class SaveFavoriteDetailPlaceUseCaseImpl(private val placeUIProvider: PlaceDetailUIObjectBox) :
    SaveFavoriteDetailPlaceUseCase {

    override suspend operator fun invoke(placeDetailsUI: PlaceDetailUI) {
        if (placeUIProvider.getDetailsPlace(placeDetailsUI.frsqId ?: "") == null) {
            placeUIProvider.saveDetailsPlace(placeDetailsUI)
        }
    }
}

interface SaveFavoriteDetailPlaceUseCase {
    suspend operator fun invoke(placeDetailsUI: PlaceDetailUI)
}