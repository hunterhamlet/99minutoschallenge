package com.hamon.provider.usecases.networking

import com.hamon.provider.networking.actions.APIActions
import com.hamon.provider.repository.PlaceRepository

internal class GetPlacesUseCasesImpl(private val repository: PlaceRepository) : GetPlacesUseCase {
    override suspend operator fun invoke(lat: Double, lng: Double): APIActions =
        repository.getPlaces(lat, lng)
}

interface GetPlacesUseCase {
    suspend operator fun invoke(lat: Double, lng: Double): APIActions
}
