package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceDetailUIObjectBox

internal class RemoveFavoriteDetailPlaceUseCaseImpl(private val placeUIProvider: PlaceDetailUIObjectBox) :
    RemoveFavoriteDetailPlaceUseCase {
    override suspend fun invoke(frsqId: String) {
        val placeRemoving = placeUIProvider.getDetailsPlace(frsqId ?: "")
        placeRemoving?.let {
            placeUIProvider.removePlaceDetails(it.id)
        }
    }

}

interface RemoveFavoriteDetailPlaceUseCase {
    suspend operator fun invoke(frsqId: String)
}