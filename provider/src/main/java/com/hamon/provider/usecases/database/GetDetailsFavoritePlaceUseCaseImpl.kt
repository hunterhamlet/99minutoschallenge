package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceDetailUIObjectBox
import com.hamon.provider.networking.actions.APIActions
import com.hamon.provider.uiModels.PlaceDetailUI

internal class GetDetailsFavoritePlaceUseCaseImpl(private val placeUIProvider: PlaceDetailUIObjectBox) :
    GetDetailsFavoritePlaceUseCase {
    override suspend fun invoke(frsqId: String): PlaceDetailUI? = placeUIProvider.getDetailsPlace(frsqId)

}

interface GetDetailsFavoritePlaceUseCase {
    suspend operator fun invoke(frsqId: String): PlaceDetailUI?
}