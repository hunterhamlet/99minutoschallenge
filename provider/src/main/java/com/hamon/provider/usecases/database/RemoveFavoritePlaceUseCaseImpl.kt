package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceUIObjectBox
import com.hamon.provider.uiModels.PlaceUI

internal class RemoveFavoritePlaceUseCaseImpl(private val placeUIProvider: PlaceUIObjectBox) :
    RemoveFavoritePlaceUseCase {
    override suspend fun invoke(place: PlaceUI) {
        val placeForRemove = placeUIProvider.getPlace(place.frsqId ?: "")
        placeForRemove?.let {
            placeUIProvider.removePlace(placeId = it.id)
        }
    }
}

interface RemoveFavoritePlaceUseCase {
    suspend operator fun invoke(place: PlaceUI)
}