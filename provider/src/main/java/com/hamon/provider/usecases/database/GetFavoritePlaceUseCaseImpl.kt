package com.hamon.provider.usecases.database

import com.hamon.provider.database.PlaceUIObjectBox
import com.hamon.provider.uiModels.PlaceUI

internal class GetFavoritePlaceUseCaseImpl(private val placeUIProvider: PlaceUIObjectBox) :
    GetFavoritePlaceUseCase {
    override suspend operator fun invoke(frsqId: String): PlaceUI? =
        placeUIProvider.getPlace(frsqId)
}

interface GetFavoritePlaceUseCase {
    suspend operator fun invoke(frsqId: String): PlaceUI?
}