package com.hamon.provider.uiModels

import android.os.Parcelable
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class PlaceDetailUI(

    //info
    @Id var id: Long = 0,
    val frsqId: String? = null,
    val name: String? = null,
    val urlImage: String? = null,

    //location
    val country: String? = null,
    val crossStreet: String? = null,
    val formattedAddress: String? = null,
    val locality: String? = null,
    val postcode: String? = null,
    val region: String? = null,

    //categories
    val categories: List<CategoryUI>? = null,

    //geocodes
    val lat: Double? = null,
    val lng: Double? = null

) : Parcelable