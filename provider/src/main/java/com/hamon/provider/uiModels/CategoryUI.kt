package com.hamon.provider.uiModels

import android.os.Parcelable
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import kotlinx.parcelize.Parcelize


@Entity
@Parcelize
data class CategoryUI(
    @Id var id: Long = 0,
    val name: String? = null,
    val iconUrl: String? = null
) : Parcelable