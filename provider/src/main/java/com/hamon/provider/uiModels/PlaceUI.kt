package com.hamon.provider.uiModels

import android.os.Parcelable
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity
data class PlaceUI(
    @Id var id: Long = 0,
    val frsqId: String? = null,
    val distance: Int? = null,
    val timezone: String? = null,
    val URLIcon: String? = null,
    val name: String? = null,
    val lat: Double? = null,
    val lng: Double? = null,
    var isFavorite: Boolean = false
) : Parcelable