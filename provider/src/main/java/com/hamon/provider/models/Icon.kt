package com.hamon.provider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Icon(
    @field:SerializedName("prefix") val prefix: String? = null,
    @field:SerializedName("suffix") val suffix: String? = null
) : Parcelable {
    fun getURLImage() = "${prefix}100${suffix}"
}