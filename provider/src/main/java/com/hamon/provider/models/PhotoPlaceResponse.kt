package com.hamon.provider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class PhotoPlaceResponse(
    @field:SerializedName("prefix") val prefix: String? = null,
    @field:SerializedName("width") val width: Int? = null,
    @field:SerializedName("created_at") val createdAt: String? = null,
    @field:SerializedName("id") val id: String? = null,
    @field:SerializedName("suffix") val suffix: String? = null,
    @field:SerializedName("height") val height: Int? = null
) : Parcelable {
    fun getURLPhoto(): String {
        return "${prefix}${width}x${height}${suffix}"
    }
}
