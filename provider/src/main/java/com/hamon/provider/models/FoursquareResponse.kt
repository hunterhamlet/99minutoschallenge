package com.hamon.provider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Id
import kotlinx.parcelize.Parcelize

@Parcelize
data class FoursquareResponse(
    @field:SerializedName("results") val results: List<PlaceItem?>? = null
) : Parcelable

@Parcelize
data class Roof(
    @field:SerializedName("latitude") val latitude: Double? = null,
    @field:SerializedName("longitude") val longitude: Double? = null
) : Parcelable

@Parcelize
data class PlaceItem(
    @Id var id: Long = 0,
    @field:SerializedName("fsq_id") val fsqId: String? = null,
    @field:SerializedName("distance") val distance: Int? = null,
    @field:SerializedName("timezone") val timezone: String? = null,
    @field:SerializedName("name") val name: String? = null,
    @field:SerializedName("geocodes") val geocodes: Geocodes? = null,
    @field:SerializedName("location") val location: Location? = null,
    @field:SerializedName("categories") val categories: List<CategoriesItem?>? = null,
    @field:SerializedName("census_block") val censusBlock: String? = null
) : Parcelable {

    fun getLat() = geocodes?.main?.latitude ?: 0.0

    fun getLng() = geocodes?.main?.longitude ?: 0.0

    fun getURLIcon() = categories?.first()?.icon?.getURLImage() ?: ""

}
