package com.hamon.provider.models

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class Place (@Id var id: Long = 0)