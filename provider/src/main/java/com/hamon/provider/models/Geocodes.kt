package com.hamon.provider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Geocodes(
    @field:SerializedName("roof") val roof: Roof? = null,
    @field:SerializedName("main") val main: Main? = null
) : Parcelable