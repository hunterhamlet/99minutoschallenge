package com.hamon.provider.models

import com.google.gson.annotations.SerializedName

data class PlaceDetailResponse(
	@field:SerializedName("fsq_id") val fsqId: String? = null,
	@field:SerializedName("timezone") val timezone: String? = null,
	@field:SerializedName("name") val name: String? = null,
	@field:SerializedName("geocodes") val geocodes: Geocodes? = null,
	@field:SerializedName("location") val location: Location? = null,
	@field:SerializedName("categories") val categories: List<CategoriesItem?>? = null
)
