package com.hamon.provider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Location(
    @field:SerializedName("country") val country: String? = null,
    @field:SerializedName("formatted_address") val formattedAddress: String? = null,
    @field:SerializedName("address") val address: String? = null,
    @field:SerializedName("locality") val locality: String? = null,
    @field:SerializedName("postcode") val postcode: String? = null,
    @field:SerializedName("dma") val dma: String? = null,
    @field:SerializedName("neighborhood") val neighborhood: List<String?>? = null,
    @field:SerializedName("region") val region: String? = null,
    @field:SerializedName("cross_street") val crossStreet: String? = null
) : Parcelable