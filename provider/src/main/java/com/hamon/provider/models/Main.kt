package com.hamon.provider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Main(
    @field:SerializedName("latitude") val latitude: Double? = null,
    @field:SerializedName("longitude") val longitude: Double? = null
) : Parcelable