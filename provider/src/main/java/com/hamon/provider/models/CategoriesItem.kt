package com.hamon.provider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoriesItem(
    @field:SerializedName("name") val name: String? = null,
    @field:SerializedName("icon") val icon: Icon? = null,
) : Parcelable