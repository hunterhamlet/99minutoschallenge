package com.hamon.provider.repository

import com.hamon.provider.BuildConfig
import com.hamon.provider.networking.URLServices
import com.hamon.provider.networking.actions.APIActions
import com.hamon.provider.networking.service.IPlaceServices

internal class PlaceRepository(private val api: IPlaceServices) : PlaceDatasource {

    override suspend fun getPlaces(lat: Double, lng: Double): APIActions {
        return try {
            val response = api.searchPlaces(
                BuildConfig.FORSQUARE_API_KEY,
                mapOf(Pair(URLServices.LAT_LNG_KEY, "${lat},${lng}"))
            )
            if (response.isSuccessful) {
                APIActions.OnSuccess(response = response.body())
            } else {
                APIActions.OnError(message = "")
            }
        } catch (error: Exception) {
            error.printStackTrace()
            APIActions.OnError(message = "")
        }
    }

    override suspend fun getPlaceDetails(frsqId: String): APIActions {
        return try {
            val response = api.getDetailsPlaces(BuildConfig.FORSQUARE_API_KEY, frsqId)
            if (response.isSuccessful) {
                APIActions.OnSuccess(response = response.body())
            } else {
                APIActions.OnError(message = "")
            }
        } catch (error: Exception) {
            error.printStackTrace()
            APIActions.OnError(message = error.message ?: "")
        }
    }

    override suspend fun getPlacePhoto(frsqId: String): APIActions {
        return try {
            val response = api.getPhotosPlace(BuildConfig.FORSQUARE_API_KEY, frsqId)
            if (response.isSuccessful) {
                APIActions.OnSuccess(response = response.body())
            } else {
                APIActions.OnError(message = "")
            }
        } catch (error: Exception) {
            error.printStackTrace()
            APIActions.OnError(message = "")
        }
    }

}

internal interface PlaceDatasource {

    suspend fun getPlaces(lat: Double, lng: Double): APIActions
    suspend fun getPlaceDetails(frsqId: String): APIActions
    suspend fun getPlacePhoto(frsqId: String): APIActions

}