package com.hamon.provider.database

import com.hamon.provider.uiModels.PlaceDetailUI
import com.hamon.provider.uiModels.PlaceDetailUI_

object PlaceDetailUIObjectBox {

    private val placeDetailsUIBox = ObjectBox.store.boxFor(PlaceDetailUI::class.java)

    fun saveDetailsPlace(placeUI: PlaceDetailUI) {
        placeDetailsUIBox.put(placeUI)
    }

    fun getDetailsPlace(frsqId: String): PlaceDetailUI? {
        val query =
            placeDetailsUIBox.query(PlaceDetailUI_.frsqId.equal(frsqId)).build()
        val result = query.find()
        query.close()
        return result.firstOrNull()
    }

    fun removePlaceDetails(id: Long) {
        placeDetailsUIBox.remove(id)
    }

    fun countPlacesSaved() = placeDetailsUIBox.count()

}