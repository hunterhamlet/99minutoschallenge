package com.hamon.provider.database

import com.hamon.provider.uiModels.PlaceUI
import com.hamon.provider.uiModels.PlaceUI_

object PlaceUIObjectBox {

    private val placeUIBox = ObjectBox.store.boxFor(PlaceUI::class.java)

    fun savePlace(place: PlaceUI) {
        placeUIBox.put(place)
    }

    fun getPlace(frsqId: String): PlaceUI? {
        val query = placeUIBox.query(PlaceUI_.frsqId.equal(frsqId)).build()
        val result = query.find()
        query.close()
        return result.firstOrNull()
    }

    fun getAllPlaces(): List<PlaceUI> = placeUIBox.all

    fun removePlace(placeId: Long) {
        placeUIBox.remove(placeId)
    }

    fun countPlacesSaved() = placeUIBox.count()

}